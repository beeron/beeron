import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import {endpoint} from "../common";

import AlbumTableWidget from '../widget/albumTable'

export default class AlbumTable extends React.Component {
    constructor(props) {
        super(props);
        this.editAlbum = this.editAlbum.bind(this);
        this.addAlbum = this.addAlbum.bind(this);
        this.deleteAlbum = this.deleteAlbum.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.postEdit = this.postEdit.bind(this);
        this.submitEdit = this.submitEdit.bind(this);
        this.readAlbums = this.readAlbums.bind(this);
        this.state = {items: null, editVisible: false, editName: null};
    }

    cancelEdit() {
        this.setState({editVisible: false});
    }

    postEdit() {
        this.cancelEdit();
        this.readAlbums();
    }

    submitEdit(values) {
        const editName = this.state.editName;
        if (editName == null) {
            $.ajax({
                type: "POST",
                url: endpoint+'/albums',
                data: values,
                error: function (response) {
                    alert(response.responseText);
                },
                success: this.postEdit
            });
        }
        else {
            $.ajax({
                type: "PUT",
                url: endpoint+'/album/' + editName,
                data: values,
                error: function (response) {
                    alert(response.responseText);
                },
                success: this.postEdit
            });
        }
    }

    deleteAlbum(name) {
        $.ajax({
            type: 'DELETE',
            url: endpoint+'/album/' + name,
            error: (response) => {
                alert(response);
            },
            success: this.readAlbums
        });
    }

    addAlbum() {
        this.setState({editVisible: true, editName: null});
    }

    editAlbum(name) {
        this.setState({editVisible: true, editName: name});
    }

    readAlbums() {
        fetch(endpoint+'/albums.json')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        items: result
                    });
                },
                (error) => {
                    alert(error);
                })
    }

    componentDidMount() {
        this.readAlbums();
    }

    render() {
        const {items, editVisible, editName}=this.state;
        const {loggedIn}=this.props;
        return <AlbumTableWidget
            items={items}
            loggedIn={loggedIn}
            editVisible={editVisible}
            editName={editName}
            onDeleteAlbum={this.deleteAlbum}
            onEditAlbum={this.editAlbum}
            onAddAlbum={this.addAlbum}
            onCancelEdit={this.cancelEdit}
            onSubmitEdit={this.submitEdit}
        />
    }
}

AlbumTable.propTypes={
    loggedIn: PropTypes.bool.isRequired
}
