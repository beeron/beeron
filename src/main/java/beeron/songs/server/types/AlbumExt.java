package beeron.songs.server.types;

import java.time.Duration;

public interface AlbumExt extends Album {

    String getName();

    int getTrackCount();

    Duration getLength();


}
