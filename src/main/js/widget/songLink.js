import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import {Link} from 'react-router-dom'

const SongLink=({album, name})=> {
    return (
        <Link to={album? '/album/'+album+'/'+name:'/song/'+name}>{name}</Link>
    );
}

SongLink.propTypes={
    album: PropTypes.string,
    name: PropTypes.string.isRequired
}

export default SongLink