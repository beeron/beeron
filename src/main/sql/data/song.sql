USE songs
GO

DELETE FROM song;

EXEC create_song2 'Pandog', '0:05:07', '79CjUdPn8M8';
EXEC create_song2 'Mongost', '0:04:43', 'TjYzCJ4dL7E';
EXEC create_song2 'A-Geuk', '0:05:16', 'HabRlwerCkA';
EXEC create_song2 'Giffy', '0:07:03', 'WJIg6Ez6EkI';
EXEC create_song2 'Pietersen', '0:05:39', 'aF-Pu8Z58zY';
EXEC create_song2 'Beer', '0:05:20', 'ntXvA3TcMRM';
EXEC create_song2 'Luhk-Chin', '0:06:47', 'zZd9eAvwEcw';
EXEC create_song2 'Horny', '0:07:28', 'Ac-3UUl-UA8';
EXEC create_song2 'A-Huhng', '0:07:10', 'O7mUAbNhjPs';
EXEC create_song2 'Bearon', '0:06:17', '9n72NfhhxnE';
EXEC create_song2 'King Augustine', '0:08:03', 'AWPPqdDPitM';
EXEC create_song2 'Tissura', '0:08:07', 'qOBNdDMvwAg';
EXEC create_song2 'Gangolf', '0:08:22', 'LdyQq0-D2-Y';
EXEC create_song2 'Fearon', '0:08:15', '7R_uKRxq55A';
EXEC create_song2 'Nag''s Head', '0:10:05', 'WLadSWlFZ4g';
EXEC create_song2 'Prince Jellotine', '0:13:46', 'K7nkud7u65c';
EXEC create_song2 'Moo Moo Cow', '0:05:17', '7Q3Tf0ZBGz0';
EXEC create_song2 'Smell The Fox', '0:05:38', 'mMbD8e4eOW4';
EXEC create_song2 'Onion Breath', '0:06:00', 'x-IWn-DwRxM';
EXEC create_song2 'Trunkus', '0:04:49', 'lio1aVqJRcU';
EXEC create_song2 'A-Coeur', '0:05:56', '_JKOn5UZGUA';
EXEC create_song2 'Le Poncey Eggs', '0:06:57', 'Rl7x4QViio0';
EXEC create_song2 'All Pandas Arise', '0:06:26', 'aKNVXNVpcnM';
EXEC create_song2 'Malagost', '0:32:17', '_DEli8WTkAc';
EXEC create_song2 'Dafydd', '0:04:46', 'naJ1NW08qKE';
EXEC create_song2 'Prester John', '0:11:38', 'VyhH5OwSlGU';
EXEC create_song2 'Nanako', '0:06:32', 'CBgv9YO1U0c';
EXEC create_song2 'Barton', '0:03:22', 'nvqVuuHyEqI';
EXEC create_song2 'Fellasophy', '0:07:56', '0CA5oO_Vvc0';
EXEC create_song2 'Beerius', '0:06:46', 'valRM9dDfwc';
EXEC create_song2 'Que? Vin? Key, Gran', '0:05:56', 'Msdi-h8gcXQ';
EXEC create_song2 'A-Ngauh', '0:07:10', 'jgHn00TD8II';
EXEC create_song2 'We Three Cats', '0:10:28', '9r5Yl7hp3T0';
EXEC create_song2 'Smelly Sock', '0:03:56', 'xnzLZWWU-eI';
EXEC create_song2 'Voss', '0:02:43', '9R4QX7FqH2I&t=10s';
EXEC create_song2 'Ovaltine', '0:11:20', 'hpRPS_O6TDg';
EXEC create_song2 'Justice For Hugh Janus', '0:04:26', '1SKEle8EmmY';
EXEC create_song2 'Cantine', '0:08:57', 'HMw6UE1Ytn4';
EXEC create_song2 'Hungry Ghost', '0:05:53', 'iFHReuZYRHo';
EXEC create_song2 'Ghidorah', '0:12:21', 'XSJlo-6cD2Y';
EXEC create_song2 'Fantine', '0:08:17', 'GBVHRtl3-mI';
EXEC create_song2 'A Monkey', '0:04:22', 'hKnkAUE8dlU';
EXEC create_song2 'Story Night',  '0:27:13', 'UPbyTd9qJX8';
EXEC create_song2 'Sharon''s Van Frank-n-Shine', '0:08:04', 'IjsoY1mYdBo';
EXEC create_song2 'Croc''s Temple', '0:16:11', 'YmlsqkeZPC4';
EXEC create_song2 'Pandos', '0:13:15', '0pkO8-Qc6kU';
EXEC create_song2 'Sanction', '0:05:23', 'eY0FNhEIZJU';
EXEC create_song2 'Carmen', '0:06:20', 'X0N6zDjJZWk';
EXEC create_song2 'Pecky', '0:06:25', 'bcD1VwOkg24';
EXEC create_song2 'Gippo Fayre', '0:04:17', 'lF2cu10lTto';
EXEC create_song2 'Renard', '0:07:55', 'SCymwRnWxNw';
EXEC create_song2 'Solomillo', '0:06:22', 'ZKm2H5ItZGc';
EXEC create_song2 'Bring The Bamboo In', '0:03:48', 'r8yg3Z54U4g';
EXEC create_song2 'Bobo', '0:07:07', 'L1OS77j_n3Q';
EXEC create_song2 'Schnitzel', '0:06:00', 'b5K6od5w-GY';
EXEC create_song2 'Sinister', '0:11:18', 'fdNEtDKB1YY';
EXEC create_song2 'Beerontee', '0:05:46';

GO