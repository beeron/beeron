import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Layout, Menu, Row, Col} from 'antd';
const {Header, Sider, Content} = Layout;
const {SubMenu} = Menu;

import SideMenu from './sideMenu'
import TopMenu from './../container/topMenu'
import AlbumTable from '../container/albumTable'
import SongTable from '../container/songTable'
import SongDetails from '../container/songDetails'

const Main=({userName, widths, albumUrl, songUrl, menuChoice, onLoginChange})=> {
    const loggedIn=userName!==undefined;
    return (
        <Layout>
            <Header className="header">
                <TopMenu userName={userName} onLoginChange={onLoginChange}/>
            </Header>
            <Layout>
                <Sider width={200} style={{background: '#fff'}}>
                    <SideMenu selected={menuChoice}/>
                </Sider>
                <Layout>
                    <Content>
                        <Row>
                            <Col span={widths[0]}>
                                <div>
                                    <AlbumTable loggedIn={loggedIn}/>
                                </div>
                            </Col>
                            <Col span={widths[1]}>
                                <SongTable loggedIn={loggedIn} album={albumUrl}/>
                            </Col>
                            <Col span={widths[2]}>
                                <SongDetails song={songUrl}/>,
                            </Col>
                        </Row>
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
}

Main.propTypes={
    userName: PropTypes.string,
    widths: PropTypes.arrayOf(PropTypes.number),
    albumUrl: PropTypes.string,
    songUrl: PropTypes.string,
    menuChoice: PropTypes.string.isRequired,
    onLoginChange: PropTypes.func.isRequired
}

export default Main