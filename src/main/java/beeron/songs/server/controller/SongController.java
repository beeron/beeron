package beeron.songs.server.controller;

import beeron.songs.server.form.SongForm;
import beeron.songs.server.services.SongService;
import beeron.songs.server.types.Song;
import beeron.songs.server.types.impl.SongImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class SongController extends AbstractController {

    private static final Logger logger = Logger.getLogger(SongController.class.getName());

    @Autowired
    private SongService songService;

    @GetMapping(value = "/beeron/songs", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Collection<Song> songsJson() {
        Collection<Song> songs = songService.getAllSongs();
        return songs;
    }

    @GetMapping(value = "/beeron/song/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Song songJson(@PathVariable String name) {
        Song song = songService.getSong(name);
        return song;
    }

    @PostMapping("/beeron/songs")
    public ResponseEntity<String> addSong(Model model, @ModelAttribute @Valid SongForm songForm, BindingResult bindingResult) {
//        if (!authorised(token)) {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//        }
        if (bindingResult.hasFieldErrors()) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(getError(bindingResult));
        }
        Song song = new SongImpl(songForm);
        songService.addSong(song);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/beeron/song/{name}")
    public ResponseEntity<String> updateSong(@PathVariable String name, @ModelAttribute @Valid SongForm songForm, BindingResult bindingResult) {
//        if (!authorised(token)) {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//        }
        if (bindingResult.hasFieldErrors()) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(getError(bindingResult));
        }
        Song song = new SongImpl(songForm);
        songService.updateOrAddSong(name, song);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/beeron/song/{name}")
    public ResponseEntity<String> deleteSong(@PathVariable String name) {
//        if (!authorised(token)) {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//        }
        songService.deleteSong(name);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
