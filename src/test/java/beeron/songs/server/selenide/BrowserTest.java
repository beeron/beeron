package beeron.songs.server.selenide;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.codeborne.selenide.Selenide.open;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BrowserTest {

    @LocalServerPort
    private int port;

    private static final String USER = "Beeron";

    @Before
    public void setup() {
        boolean headless = Boolean.valueOf(System.getProperty("HEADLESS"));
        Configuration.browser = headless ? WebDriverRunner.PHANTOMJS : WebDriverRunner.MARIONETTE;
        Configuration.baseUrl = "http://localhost:" + port;
    }

    @Test
    public void testBrowser() {
        open("/beeron");
//        $(By.id("name")).shouldBe(Condition.visible).setValue(USER);
//        $(By.id("submit")).click();
    }
}
