import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form} from 'antd';
import moment from 'moment';
import SongSelectWidget from '../widget/songSelect'
import {endpoint} from "../common";

class SongSelectForm extends React.Component {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.state = {items: null};
    }

    componentDidMount() {
        const album=this.props.album;
        //get songs not on the album
        fetch(endpoint+'/album/' + album + '/available/names.json')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        items: result
                    });
                },
                (error) => {
                    alert(error);
                })
    }

    submit(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.submit(values);
            }
        });
    }

    render() {
        if(this.state.items===null){
            return null;
        }
        return <SongSelectWidget form={this.props.form} submit={this.submit} cancel={this.props.cancel} items={this.state.items}/>
    }
}

SongSelectForm.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    album: PropTypes.string.isRequired
}

export default Form.create({})(SongSelectForm);

