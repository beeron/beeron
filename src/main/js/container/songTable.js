import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {endpoint} from "../common";
import SongTableWidget from '../widget/songTable'

export default class SongTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {items: null};
        this.editSong = this.editSong.bind(this);
        this.createSong = this.createSong.bind(this);
        this.addSong = this.addSong.bind(this);
        this.removeSong = this.removeSong.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.submitEdit = this.submitEdit.bind(this);
        this.postEdit = this.postEdit.bind(this);
        this.cancelSelect = this.cancelSelect.bind(this);
        this.submitSelect = this.submitSelect.bind(this);
        this.readSongs = this.readSongs.bind(this);
        this.state = {items: null, editVisible: false, selectVisible: false, editName: null, editLength: null, editYoutube: null};
    }

    cancelEdit() {
        this.setState({editVisible: false});
    }

    postEdit() {
        this.cancelEdit();
        this.readSongs(this.props.album);
    }

    submitEdit(values) {
        values.length = toSeconds(values.length);
        const {editName} = this.state;
        if (editName == null) {
            //add song to album, or create new song
            if (this.props.album == null) {
                //create new song
            }
            else {
                //add song to album
            }
        }
        else {
            //update song
            $.ajax({
                type: "PUT",
                url: endpoint+'/song/' + editName,
                data: values,
                error: function (response) {
                    alert(response.responseText);
                },
                success: this.postEdit
            });
        }
    }

    cancelSelect() {
        this.setState({selectVisible: false});
    }

    submitSelect(values){
        this.cancelSelect();
        const song=values.song;
        //add song to album
        $.ajax({
            type: "POST",
            url: endpoint+'/album/' + this.props.album,
            data: {song:song},
            error: function (response) {
                alert(response.responseText);
            },
            success: this.postEdit
        });
    }

    removeSong(){
    }

    deleteSong(event, record) {
        event.preventDefault();
        $.ajax({
            type: 'DELETE',
            url: endpoint+'/song/' + record.name,
            error: (response) => {
                alert(response);
            },
            success: this.readSongs(this.props.album)
        });
    }

    createSong() {
        this.setState({editVisible: true, editName: null, editLength: null, editYoutube: null});
    }

    editSong(event, name, length, youtube) {
        event.preventDefault();
        this.setState({
            editVisible: true,
            editName: name,
            editLength: length,
            editYoutube: youtube
        });
    }

    addSong() {
        this.setState({selectVisible: true});
    }

    readSongs(album) {
        fetch(endpoint + (album === null ? '' : '/album/' + album) + '/songs.json')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        items: result
                    });
                },
                (error) => {
                    alert(error);
                })
    }

    componentDidMount() {
        this.readSongs(this.props.album);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.album !== nextProps.album) {
            this.readSongs(nextProps.album);
        }
    }

    render() {
        const {items, editVisible, selectVisible, editName, editLength, editYoutube}=this.state;
        const {loggedIn, album}=this.props;
        return <SongTableWidget
            items={items}
            loggedIn={loggedIn}
            album={album}
            editVisible={editVisible}
            editName={editName}
            editLength={editLength}
            editYoutube={editYoutube}
            selectVisible={selectVisible}
            onRemoveSong={this.removeSong}
            onDeleteSong={this.deleteSong}
            onEditSong={this.editSong}
            onAddSong={this.addSong}
            onCreateSong={this.createSong}
            onCancelEdit={this.cancelEdit}
            onSubmitEdit={this.submitEdit}
            onCancelSelect={this.cancelSelect}
            onSubmitSelect={this.submitSelect}
        />
    }
}

SongTable.propTypes={
    loggedIn: PropTypes.bool.isRequired,
    album: PropTypes.string
}
