import ReactDOM from "react-dom";
import React from "react";

const Direction = {
    UP: 'u',
    DOWN: 'd',
    LEFT: 'l',
    RIGHT: 'r',
};
Object.freeze(Direction);

function opposite(direction) {
    switch (direction) {
        case Direction.DOWN:
            return Direction.UP;
        case Direction.UP:
            return Direction.DOWN;
        case Direction.LEFT:
            return Direction.RIGHT;
        case Direction.RIGHT:
            return Direction.LEFT;
    }
}

var keyCode;

function keyDown(event) {
    keyCode = event.keyCode;
}

$(document).keydown(event => {
    keyDown(event);
});

class Wall extends React.Component {

    render() {
        return (
            <td key={'td ' + this.props.position.toString()}><img src={'/images/' + this.props.image + '.png'}/></td>
        );
    }

}

class Position {
    constructor(x, y, dx = 0, dy = 0) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    clone() {
        return new Position(this.x, this.y, this.dx, this.dy);
    }

    cloneWithoutDelta() {
        return new Position(this.x, this.y);
    }

    adjacent(direction) {
        var newPosition = this.cloneWithoutDelta();
        switch (direction) {
            case Direction.UP:
                if (this.dx == 0) {
                    --newPosition.x;
                }
                break;
            case Direction.DOWN:
                ++newPosition.x;
                break;
            case Direction.LEFT:
                if (this.dy == 0) {
                    --newPosition.y;
                }
                break;
            case Direction.RIGHT:
                ++newPosition.y;
                break;
        }
        return newPosition;
    }

    above(position) {
        return this.x < position.x || (this.x == position.x && this.dx < position.dx);
    }

    below(position) {
        return this.x > position.x || (this.x == position.x && this.dx > position.dx);
    }

    toLeftOf(position) {
        return this.y < position.y || (this.y == position.y && this.dy < position.dy);
    }

    toRightOf(position) {
        return this.y > position.y || (this.y == position.y && this.dy > position.dy);
    }

    move(direction) {
        var newPosition = this.clone();
        switch (direction) {
            case Direction.UP:
                newPosition.up();
                break;
            case Direction.DOWN:
                newPosition.down();
                break;
            case Direction.LEFT:
                newPosition.left();
                break;
            case Direction.RIGHT:
                newPosition.right();
                break;
        }
        return newPosition;
    }

    spread(position) {
        if (this.equalsWithoutDelta(position)) {
            return true;
        }
        if (this.dx > 0 && position.x == this.x + 1 && position.y == this.y) {
            return true;
        }
        if (this.dy > 0 && position.x == this.x && position.y == this.y + 1) {
            return true;
        }
        return false;
    }

    covering() {
        if (this.dx > 3) {
            return new Position(this.x + 1, this.y);
        }
        if (this.dy > 3) {
            return new Position(this.x, this.y + 1);
        }
        return new Position(this.x, this.y);
    }

    overlaps(position) {
        let totalx1 = this.x * 8 + this.dx;
        let totaly1 = this.y * 8 + this.dy;
        let totalx2 = position.x * 8 + position.dx;
        let totaly2 = position.y * 8 + position.dy;
        return totalx1 > totalx2 - 8 && totalx1 < totalx2 + 8 && totaly1 > totaly2 - 8 && totaly1 < totaly2 + 8;
    }

    overlapsWithOffset(position, xoffset) {
        return this.y == position.y && this.x > position.x - xoffset && this.y < position.x + xoffset;
    }

    directionTo(position) {
        if (position.x < this.x) {
            return Direction.UP;
        }
        if (position.x > this.x) {
            return Direction.DOWN;
        }
        if (position.y < this.y) {
            return Direction.LEFT;
        }
        if (position.y > this.y) {
            return Direction.RIGHT;
        }
        if (position.dx < this.dx) {
            return Direction.UP;
        }
        if (position.dx > this.dx) {
            return Direction.DOWN;
        }
        if (position.dy < this.dy) {
            return Direction.LEFT;
        }
        if (position.dy > this.dy) {
            return Direction.RIGHT;
        }
        return null;
    }

    flip(maxY) {
        var newPosition = this.cloneWithoutDelta();
        if (newPosition.y == 0) {
            newPosition.y = maxY - 1;
        }
        else {
            newPosition.y = 0;
        }
        return newPosition;
    }

    hasDelta() {
        return this.dx != 0 || this.dy != 0;
    }

    up() {
        --this.dx;
        if (this.dx < 0) {
            this.dx = 7;
            --this.x;
        }
    }

    down() {
        ++this.dx;
        if (this.dx > 7) {
            this.dx = 0;
            ++this.x;
        }
    }

    left() {
        --this.dy;
        if (this.dy < 0) {
            this.dy = 7;
            --this.y;
        }
    }

    right() {
        ++this.dy;
        if (this.dy > 7) {
            this.dy = 0;
            ++this.y;
        }
    }

    equals(position) {
        return this.x == position.x && this.y == position.y && this.dx == position.dx && this.dy == position.dy;
    }

    equalsWithoutDelta(position) {
        return this.x == position.x && this.y == position.y;
    }

}

Position.prototype.toString = function positionToString() {
    return this.x + ', ' + this.y + ' (' + this.dx + ', ' + this.dy + ')';
}

class Person {

    constructor(position, offsideCount) {
        this.position = position;
        this.offsideCount = offsideCount;
    }

    offside() {
        return this.offsideCount > 0;
    }

}

class Pacman extends Person {
    constructor(position, offsideCount, direction, bittenCount = 0) {
        super(position == null ? Pacman.homePosition() : position, offsideCount);
        this.direction = direction == null ? Direction.RIGHT : direction;
        this.bittenCount = bittenCount;
    }

    static homePosition() {
        return new Position(12, 11);
    }

    static bittenTime() {
        return 50;
    }

    newDirection(grid) {
        var newDirection = this.direction;
        if (keyCode >= 37 && keyCode <= 40) {
            const hasDelta = this.position.hasDelta();
            var attemptedDirection;
            if (keyCode === 38 && (newDirection == Direction.DOWN || !hasDelta)) {
                attemptedDirection = Direction.UP;
            }
            else if (keyCode === 39 && (newDirection == Direction.LEFT || !hasDelta)) {
                attemptedDirection = Direction.RIGHT;
            }
            else if (keyCode === 40 && (newDirection == Direction.UP || !hasDelta)) {
                attemptedDirection = Direction.DOWN;
            }
            else if (keyCode === 37 && (newDirection == Direction.RIGHT || !hasDelta)) {
                attemptedDirection = Direction.LEFT;
            }
            if (attemptedDirection != null) {
                const canMove = grid.canMove(this.position, attemptedDirection);
                if (canMove) {
                    newDirection = attemptedDirection;
                    keyCode = null;
                }
            }
        }
        return newDirection;
    }

    move(grid, ghosts, powerCount, bitten) {
        var newPosition = this.position;
        var newOffsideCount = this.offsideCount;
        var newDirection = this.direction;
        var newBittenCount = this.bittenCount;
        if (newBittenCount == 0 && powerCount == 0) {
            for (let i = 0; i < ghosts.length; ++i) {
                if (ghosts[i].position.overlaps(this.position)) {
                    newBittenCount = Pacman.bittenTime();
                }
            }
        }
        if (newBittenCount > 0) {
            --newBittenCount;
            if (newBittenCount == 0) {
                let ghostNearHome = false;
                ghosts.forEach(
                    ghost => {
                        if (ghost.position.overlapsWithOffset(Pacman.homePosition(), 3)) {
                            ghostNearHome = true;
                        }
                    }
                );
                if (ghostNearHome) {
                    newBittenCount = 1;
                }
                else {
                    newPosition = Pacman.homePosition();
                    newDirection = Direction.RIGHT;
                    bitten();
                }
            }
        }
        else if (newOffsideCount > 0) {
            --newOffsideCount;
            if (newOffsideCount == 0) {
                newPosition = newPosition.flip(grid.rowLength(this.position.x));
            }
        }
        else {
            newDirection = this.newDirection(grid);
            const adjacent = this.position.adjacent(newDirection);
            if (!grid.inside(adjacent)) {
                newOffsideCount = 20;
            }
            else if (grid.navigable(adjacent)) {
                newPosition = newPosition.move(newDirection);
            }
        }
        return new Pacman(newPosition, newOffsideCount, newDirection, newBittenCount);
    }

}

class Ghost extends Person {

    constructor(position, offsideCount, home, startingPosition, target = null, goingHome = false) {
        super(position, offsideCount);
        this.home = home;
        this.startingPosition = startingPosition;
        this.target = target;
        this.goingHome = goingHome;
    }

    static homePosition() {
        return new Position(10, 11);
    }

    static homeTarget() {
        return new Position(8, 11);
    }

    direction() {
        if (this.target == null) {
            if (this.position.y == 0) {
                return Direction.LEFT;
            }
            return Direction.RIGHT;
        }
        if (this.target.below(this.position)) {
            return Direction.DOWN;
        }
        if (this.target.above(this.position)) {
            return Direction.UP;
        }
        if (this.target.toLeftOf(this.position)) {
            return Direction.LEFT;
        }
        if (this.target.toRightOf(this.position)) {
            return Direction.RIGHT;
        }
        return null;
    }

    static getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    move(grid, pacman, powerCount) {
        var newPosition = this.position;
        var newOffsideCount = this.offsideCount;
        var newHome = this.home;
        var newTarget = this.target;
        var newGoingHome = this.goingHome;
        if (newHome > 0) {
            --newHome;
            if (newHome == 0) {
                newTarget = Ghost.homePosition();
            }
        }
        else if (newOffsideCount > 0) {
            --newOffsideCount;
            if (newOffsideCount == 0) {
                newPosition = newPosition.flip(grid.rowLength(this.position.x));
                newTarget = grid.nextChoice(newPosition, newPosition.y == 0 ? Direction.RIGHT : Direction.LEFT);
            }
        }
        else {
            if (powerCount != 0) {
                if (!newGoingHome && this.position.overlaps(pacman.position)) {
                    newGoingHome = true;
                }
            }
            else if (powerCount == Game.powerCount()) {
                //Pacman has just eaten a power
                if (grid.visible(newPosition, pacman.position)) {
                    //Pacman is visible so make ghost go in opposite direction
                    let pacmanDirection = newPosition.directionTo(pacman.position);
                    let possibleDirections = grid.possibleDirections(newPosition);
                    let desiredDirection = opposite(pacmanDirection);
                    for (let i = 0; i < possibleDirections.length; ++i) {
                        if (possibleDirections[i] == desiredDirection) {
                            newTarget = grid.nextChoice(newPosition, desiredDirection);
                        }
                    }
                }
            }
            let direction = null;
            if (newGoingHome) {
                if(newPosition.equals(this.startingPosition)){
                    newGoingHome=false;
                    newHome=50;
                }
                else{
                    direction = newPosition.directionTo(this.startingPosition);
                }
            }
            else {
                direction = this.direction();
            }
            if (direction != null) {
                const adjacent = this.position.adjacent(direction);
                if (!grid.inside(adjacent)) {
                    newOffsideCount = 15;
                }
                else {
                    newPosition = newPosition.move(direction);
                    if (!newGoingHome && newTarget != null && newPosition.equals(newTarget)) {
                        if (newTarget.equals(Ghost.homePosition())) {
                            newTarget = Ghost.homeTarget();
                        }
                        else {
                            let chosenDirection = null;
                            //check if the ghost can see Pacman
                            let pacmanVisibleDirection = null;
                            if (pacman != null && grid.visible(newPosition, pacman.position)) {
                                pacmanVisibleDirection = newPosition.directionTo(pacman.position);
                                if (powerCount == 0) {
                                    //if Pacman is not powered up, head towards his current position
                                    chosenDirection = pacmanVisibleDirection;
                                    newTarget = new Position(pacman.position.x, pacman.position.y);
                                }
                            }
                            if (chosenDirection == null) {
                                //normally don't choose the opposite direction just travelled
                                let excludeDirection = opposite(direction);
                                if (powerCount != 0 && pacmanVisibleDirection != null) {
                                    //but if Pacman is powered up and visible, avoid that direction
                                    excludeDirection = pacmanVisibleDirection;
                                }
                                let possibleDirections = grid.possibleDirections(newPosition, excludeDirection);
                                let choice = Ghost.getRandomInt(0, possibleDirections.length - 1);
                                chosenDirection = possibleDirections[choice];
                                newTarget = grid.nextChoice(newPosition, chosenDirection);
                            }
                            if (newTarget.equals(this.target)) {
                                newTarget = null;
                            }
                        }
                    }
                }
            }
        }
        return new Ghost(newPosition, newOffsideCount, newHome, this.startingPosition, newTarget, newGoingHome);
    }

}

class Maze extends React.Component {

    constructor(props) {
        super(props);
        this.state = {pill: props.pill, power: props.power};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pacman != null && (this.state.pill || this.state.power)) {
            const hasPacmanCovering = nextProps.position.equalsWithoutDelta(nextProps.pacman.position.covering());
            if (hasPacmanCovering) {
                if (this.state.pill) {
                    this.setState({pill: false});
                    this.props.pillEaten();
                }
                if (this.state.power) {
                    this.setState({power: false});
                    this.props.powerEaten();
                }
            }
        }
    }

    render() {
        const {position, pacman, ghosts, gate, powerCount}=this.props;
        const {pill, power}=this.state;
        let hasPacman = pacman != null && !pacman.offside() && position.equalsWithoutDelta(pacman.position);
        let hasGhost = false;
        let hasGhostSpread = false;
        let icon = null;
        let ghostIcon=null;
        for (let i = 0; i < ghosts.length; ++i) {
            if (!ghosts[i].offside()) {
                if (ghosts[i].position.spread(position)) {
                    hasGhostSpread = true;
                }
                if (position.equalsWithoutDelta(ghosts[i].position)) {
                    hasGhost = true;
                    var leftGhost = (ghosts[i].position.dy * 4) + 'px';
                    var topGhost = (ghosts[i].position.dx * 4) + 'px';
                    if(ghosts[i].goingHome){
                        ghostIcon="/images/AGeyes.png";
                    }
                    else if(powerCount==0){
                        ghostIcon="/images/AG.png";
                    }
                    else{
                        ghostIcon="/images/AGy.png";
                    }
                }
            }
        }
        if (hasPacman) {
            if (pacman.bittenCount > 0) {
                let bitten = Math.floor((Pacman.bittenTime() - pacman.bittenCount) / 2) + 1;
                if (bitten > 0 && bitten < 10) {
                    icon = Direction.UP + bitten;
                }
                else {
                    hasPacman = false;
                }
            }
            else {
                icon = pacman.direction == Direction.LEFT || pacman.direction == Direction.RIGHT ? ((pacman.position.y % 2) * 8 + pacman.position.dy) : ((pacman.position.x % 2) * 8 + pacman.position.dx);
                if (icon <= 8) {
                    icon += 1;
                }
                else {
                    icon -= ((icon - 9) * 2) + 1;
                }
                icon = pacman.direction + icon;
            }
            var leftPacman = (pacman.position.dy * 4) + 'px';
            var topPacman = (pacman.position.dx * 4) + 'px';
        }
        return (
            <td key={'td ' + this.props.position.toString()}>
                {!hasPacman && !hasGhostSpread && pill &&
                <img src="/images/pill.png" style={{position: 'relative'}}/>
                }
                {!hasPacman && !hasGhostSpread && power &&
                <img src="/images/power.png" style={{position: 'relative'}}/>
                }
                {!hasGhostSpread && !hasPacman && gate &&
                <img src="/images/gate.png" style={{position: 'relative'}}/>
                }
                {hasGhost && !hasPacman &&
                <img src={ghostIcon}
                     style={{position: 'relative', left: leftGhost, top: topGhost}}/>
                }
                {hasPacman &&
                <img src={"/images/pm" + icon + ".png"}
                     style={{position: 'relative', left: leftPacman, top: topPacman}}/>
                }
            </td>
        );
    }

}

class Grid {
    constructor(rows) {
        this.rows = rows;
    }

    navigable(position, allowOffside = false) {
        if (!this.inside(position)) {
            return allowOffside;
        }
        return this.rows[position.x][position.y].props.navigable;
    }

    inside(position) {
        return position.x >= 0 && position.y >= 0 && position.x < this.rows.length && position.y < this.rows[position.x].length;
    }

    visible(from, to) {
        if (from.x != to.x && from.y != to.y) {
            return false;
        }
        var current = new Position(from.x, from.y);
        if (from.x == to.x) {
            while (current.y != to.y) {
                if (!this.navigable(current)) {
                    return false;
                }
                if (from.y < to.y) {
                    ++current.y;
                }
                else {
                    --current.y;
                }
            }
        }
        else {
            while (current.x != to.x) {
                if (!this.navigable(current)) {
                    return false;
                }
                if (from.x < to.x) {
                    ++current.x;
                }
                else {
                    --current.x;
                }
            }
        }
        return true;
    }

    nextChoice(start, direction) {
        var current = new Position(start.x, start.y);
        while (true) {
            var next = new Position(current.x, current.y);
            switch (direction) {
                case Direction.UP:
                    --next.x;
                    break;
                case Direction.DOWN:
                    ++next.x;
                    break;
                case Direction.LEFT:
                    --next.y;
                    break;
                case Direction.RIGHT:
                    ++next.y;
                    break;
            }
            if (!this.navigable(next)) {
                return current;
            }
            current = next;
            let possibleDirections = this.possibleDirections(current);
            if (possibleDirections.length > 2) {
                return current;
            }
        }
    }

    possibleDirections(position, exclude = null) {
        var possibleDirections = [];
        if (exclude != Direction.DOWN && this.navigable(new Position(position.x + 1, position.y), true)) {
            possibleDirections.push(Direction.DOWN);
        }
        if (exclude != Direction.UP && this.navigable(new Position(position.x - 1, position.y), true)) {
            possibleDirections.push(Direction.UP);
        }
        if (exclude != Direction.LEFT && this.navigable(new Position(position.x, position.y - 1), true)) {
            possibleDirections.push(Direction.LEFT);
        }
        if (exclude != Direction.RIGHT && this.navigable(new Position(position.x, position.y + 1), true)) {
            possibleDirections.push(Direction.RIGHT);
        }
        return possibleDirections;
    }

    canMove(position, direction) {
        var adjacent = position.adjacent(direction);
        if (this.inside(adjacent)) {
            return this.rows[adjacent.x][adjacent.y].props.navigable;
        }
        return false;
    }

    rowLength(rowIndex) {
        return this.rows[rowIndex].length;
    }

}

class Status extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {score, lives}=this.props;
        const livesElements = [];
        for (let i = 0; i < lives; ++i) {
            livesElements.push(<span key={'lives' + i}><img
                style={{padding: '5px', position: 'relative', display: 'inline-block'}}
                src='/images/pmr9.png'/></span>);
        }
        return (
            <div style={{position: 'relative', display: 'inline-block'}}>
                    <span style={{fontWeight: 'bold', verticalAlign: 'top'}}>
                        SCORE:{String("00000" + score).slice(-5)}
                    </span>
                {lives == 0 ? (
                    <span style={{
                        fontWeight: 'bold',
                        verticalAlign: 'top',
                        padding: '5px'
                    }}>GAME OVER</span>) : livesElements}
            </div>
        );
    }
}

class Game extends React.Component {

    constructor() {
        super();
        this.state = {
            pacman: new Pacman(null, 0, null),
            score: 0,
            lives: 3,
            powerCount: 0,
            ghosts: [
                new Ghost(new Position(10, 9), 0, 80, new Position(10, 9)),
                new Ghost(new Position(10, 10), 0, 40, new Position(10, 10)),
                new Ghost(new Position(10, 12), 0, 20, new Position(10, 12)),
                new Ghost(new Position(10, 13), 0, 60, new Position(10, 13))
            ]
        };
        this.move = this.move.bind(this);
        this.pillEaten = this.pillEaten.bind(this);
        this.powerEaten = this.powerEaten.bind(this);
        this.bitten = this.bitten.bind(this);
    }

    static powerCount() {
        return 200;
    }

    componentDidMount() {
        this.timer = setInterval(this.move, 100);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    bitten() {
        this.setState((prevState, props) => ({
            lives: prevState.lives - 1
        }));
    }

    pillEaten() {
        this.setState((prevState, props) => ({
            score: prevState.score + 10
        }));
    }

    powerEaten() {
        this.setState((prevState, props) => ({
            score: prevState.score + 50,
            powerCount: Game.powerCount()
        }));
    }

    move() {
        this.setState(function (prevState, props) {
            let newLives = prevState.lives;
            let newPowerCount = prevState.powerCount;
            const newPacman = prevState.pacman == null ? null : prevState.pacman.move(this.grid, prevState.ghosts, newPowerCount, () => --newLives);
            let newGhosts = [];
            for (let i = 0; i < prevState.ghosts.length; ++i) {
                newGhosts.push(prevState.ghosts[i].move(this.grid, newPacman, newPowerCount));
            }
            if (newPowerCount > 0) {
                --newPowerCount;
            }
            return {
                pacman: newLives > 0 ? newPacman : null,
                ghosts: newGhosts,
                lives: newLives,
                powerCount: newPowerCount
            };
        });
    }

    createCell(position, type) {
        switch (type) {
            case 'TL':
                return <Wall position={position} key={position.toString()} image={'bartl'}/>;
            case 'TR':
                return <Wall position={position} key={position.toString()} image={'bartr'}/>;
            case 'BL':
                return <Wall position={position} key={position.toString()} image={'barbl'}/>;
            case 'BR':
                return <Wall position={position} key={position.toString()} image={'barbr'}/>;
            case 'BT':
                return <Wall position={position} key={position.toString()} image={'barhb'}/>;
            case 'VT':
                return <Wall position={position} key={position.toString()} image={'barvt'}/>;
            case 'HT':
                return <Wall position={position} key={position.toString()} image={'barht'}/>;
            case 'VB':
                return <Wall position={position} key={position.toString()} image={'barvb'}/>;
            case 'HL':
                return <Wall position={position} key={position.toString()} image={'barhl'}/>;
            case 'HR':
                return <Wall position={position} key={position.toString()} image={'barhr'}/>;
            case 'LF':
                return <Wall position={position} key={position.toString()} image={'barl'}/>;
            case 'RT':
                return <Wall position={position} key={position.toString()} image={'barr'}/>;
            case '-':
                return <Wall position={position} key={position.toString()} image={'barh'}/>;
            case '|':
                return <Wall position={position} key={position.toString()} image={'barv'}/>;
            case ' ':
                return this.createMazeCell(position, false, false, false);
            case '.':
                return this.createMazeCell(position, true, false, false);
            case 'o':
                return this.createMazeCell(position, false, true, false);
            case 'x':
                return this.createMazeCell(position, false, false, true);
            default:
                return null;
        }
    }

    createMazeCell(position, pill, power, gate) {
        return <Maze navigable={true} position={position} direction={this.state.direction}
                     key={position.toString()} pacman={this.state.pacman} ghosts={this.state.ghosts}
                     pill={pill} power={power} gate={gate} pillEaten={this.pillEaten} powerEaten={this.powerEaten} powerCount={this.state.powerCount}/>;

    }

    createRow(rowNum, rowDescription) {
        var row = [];
        var cellDescription = "";
        while (true) {
            if (rowDescription.length == 0) {
                return row;
            }
            cellDescription = cellDescription + rowDescription.charAt(0);
            rowDescription = rowDescription.substr(1);
            let position = new Position(rowNum, row.length);
            var cell = this.createCell(position, cellDescription);
            if (cell != null) {
                cellDescription = "";
                row.push(cell);
            }
        }
    }

    createMaze(mazeDescription) {
        var rows = [];
        for (let i = 0; i < mazeDescription.length; ++i) {
            rows.push(this.createRow(i, mazeDescription[i]));
        }
        this.grid = new Grid(rows);
    }

    render() {
        const {score, lives}=this.state;
        const mazeDescription = [
            'TL----------HT----------TR',
            '|..........|..........|',
            '|oTL-TR.TL--TR.|.TL--TR.TL-TRo|',
            '|.BL-BR.BL--BR.VB.BL--BR.BL-BR.|',
            '|.....................|',
            '|.HL-HR.VT.HL--HT--HR.VT.HL-HR.|',
            '|.....|....|....|.....|',
            'BL---TR.LF--HR VB HL--RT.TL---BR',
            '    |.|         |.|    ',
            '----BR.VB TL--x--TR VB.BL----',
            '     .  |     |  .     ',
            '----TR.VT BL-----BR VT.TL----',
            '    |.|         |.|    ',
            'TL---BR.VB HL--HT--HR VB.BL---TR',
            '|..........|..........|',
            '|.HL-TR.HL--HR.VB.HL--HR.TL-HR.|',
            '|...|.............|...|',
            'LF-HR.VB.VT.HL--HT--HR.VT.VB.HL-RT',
            '|.....|....|....|.....|',
            '|oHL---BT--HR.VB.HL--BT---HRo|',
            '|.....................|',
            'BL---------------------BR',
        ];
        this.createMaze(mazeDescription);
        const mazeElements = this.grid.rows.map((row, i) => <tr key={'row' + i}>{row}</tr>);
        return (
            <div>
                <Status score={score} lives={lives}/>
                <table className="pacman">
                    <tbody>
                    {mazeElements}
                    </tbody>
                </table>
            </div>
        );
    }

}

ReactDOM
    .render(
        <Game/>,
        document
            .getElementById(
                'root'
            )
    )
;