USE songs
GO

DELETE FROM album;

EXEC create_album 'Volume 1';
EXEC create_album 'Volume 2';
EXEC create_album 'Volume 3';
EXEC create_album 'Volume 4';
EXEC create_album 'Volume 5';
EXEC create_album 'Volume 6';
EXEC create_album 'Volume 7';
EXEC create_album 'Volume 8';
EXEC create_album 'Greatest Hits';

EXEC add_song_to_album 'Volume 1', 1, 'Pandog';
EXEC add_song_to_album 'Volume 1', 2, 'Mongost';
EXEC add_song_to_album 'Volume 1', 3, 'A-Geuk';
EXEC add_song_to_album 'Volume 1', 4, 'Giffy';
EXEC add_song_to_album 'Volume 1', 5, 'Pietersen';
EXEC add_song_to_album 'Volume 1', 6, 'Beer';
EXEC add_song_to_album 'Volume 1', 7, 'Luhk-Chin';
EXEC add_song_to_album 'Volume 1', 8, 'Horny';
EXEC add_song_to_album 'Volume 1', 9, 'A-Huhng';
EXEC add_song_to_album 'Volume 1', 10, 'Bearon';
EXEC add_song_to_album 'Volume 2', 1, 'King Augustine';
EXEC add_song_to_album 'Volume 2', 2, 'Tissura';
EXEC add_song_to_album 'Volume 2', 3, 'Gangolf';
EXEC add_song_to_album 'Volume 2', 4, 'Fearon';
EXEC add_song_to_album 'Volume 2', 5, 'Nag''s Head';
EXEC add_song_to_album 'Volume 2', 6, 'Prince Jellotine';
EXEC add_song_to_album 'Volume 2', 7, 'Moo Moo Cow';
EXEC add_song_to_album 'Volume 3', 1, 'Smell The Fox';
EXEC add_song_to_album 'Volume 3', 2, 'Onion Breath';
EXEC add_song_to_album 'Volume 3', 3, 'Trunkus';
EXEC add_song_to_album 'Volume 3', 4, 'A-Coeur';
EXEC add_song_to_album 'Volume 3', 5, 'Le Poncey Eggs';
EXEC add_song_to_album 'Volume 3', 6, 'All Pandas Arise';
EXEC add_song_to_album 'Volume 3', 7, 'Malagost';
EXEC add_song_to_album 'Volume 4', 1, 'Dafydd';
EXEC add_song_to_album 'Volume 4', 2, 'Prester John';
EXEC add_song_to_album 'Volume 4', 3, 'Nanako';
EXEC add_song_to_album 'Volume 4', 4, 'Barton';
EXEC add_song_to_album 'Volume 4', 5, 'Fellasophy';
EXEC add_song_to_album 'Volume 4', 6, 'Beerius';
EXEC add_song_to_album 'Volume 4', 7, 'Que? Vin? Key, Gran';
EXEC add_song_to_album 'Volume 4', 8, 'A-Ngauh';
EXEC add_song_to_album 'Volume 4', 9, 'We Three Cats';
EXEC add_song_to_album 'Volume 5', 1, 'Smelly Sock';
EXEC add_song_to_album 'Volume 5', 2, 'Voss';
EXEC add_song_to_album 'Volume 5', 3, 'Ovaltine';
EXEC add_song_to_album 'Volume 5', 4, 'Justice For Hugh Janus';
EXEC add_song_to_album 'Volume 5', 5, 'Cantine';
EXEC add_song_to_album 'Volume 5', 6, 'Hungry Ghost';
EXEC add_song_to_album 'Volume 5', 7, 'Ghidorah';
EXEC add_song_to_album 'Volume 5', 8, 'Fantine';
EXEC add_song_to_album 'Volume 5', 9, 'A Monkey';
EXEC add_song_to_album 'Volume 6', 1, 'Story Night';
EXEC add_song_to_album 'Volume 6', 2, 'Sharon''s Van Frank-n-Shine';
EXEC add_song_to_album 'Volume 6', 3, 'Croc''s Temple';
EXEC add_song_to_album 'Volume 6', 4, 'Pandos';
EXEC add_song_to_album 'Volume 7', 1, 'Sanction';
EXEC add_song_to_album 'Volume 7', 2, 'Carmen';
EXEC add_song_to_album 'Volume 7', 3, 'Pecky';
EXEC add_song_to_album 'Volume 7', 4, 'Gippo Fayre';
EXEC add_song_to_album 'Volume 7', 5, 'Renard';
EXEC add_song_to_album 'Volume 7', 6, 'Solomillo';
EXEC add_song_to_album 'Volume 7', 7, 'Bring The Bamboo In';
EXEC add_song_to_album 'Volume 7', 8, 'Bobo';
EXEC add_song_to_album 'Volume 7', 9, 'Schnitzel';
EXEC add_song_to_album 'Volume 7', 10, 'Beerontee';

GO