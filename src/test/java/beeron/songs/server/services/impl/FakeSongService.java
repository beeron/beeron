package beeron.songs.server.services.impl;

import beeron.songs.server.services.SongService;
import beeron.songs.server.types.Song;
import beeron.songs.server.types.impl.SongImpl;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;

public class FakeSongService implements SongService {

    private static final Song[] SONGS = new Song[]{new SongImpl("Pandog", Duration.ofMinutes(5)), new SongImpl("Mongost", Duration.ofMinutes(4))};

    @Override
    public Collection<Song> getAllSongs() {
        return Arrays.asList(SONGS);
    }

    @Override
    public Song getSong(String name) {
        return SONGS[0];
    }

    @Override
    public void addSong(Song song) {
    }

    @Override
    public void updateOrAddSong(String name, Song song) {
    }

    @Override
    public void deleteSong(String name) {
    }
}
