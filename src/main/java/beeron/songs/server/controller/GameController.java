package beeron.songs.server.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class GameController extends AbstractController {

    private static final Logger logger = Logger.getLogger(GameController.class.getName());

    @GetMapping("/beeron/pacman")
    public String login(Model model, HttpServletResponse response) {
        return "pacman";
    }

}
