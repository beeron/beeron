import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Route, Switch} from 'react-router-dom'
import Cookies from 'universal-cookie';

import MainView from '../widget/main'

export default class Main extends React.Component {

    constructor() {
        super();
        this.onLoginChange = this.onLoginChange.bind(this);
        const cookies = new Cookies();
        const userName=cookies.get('userName');
        this.state = {
            userName: userName,
        };
    }

    onLoginChange(userName) {
        const cookies = new Cookies();
        cookies.set('userName', userName, { path: '/' });
        this.setState({userName: userName});
    }

    mainView(widths, albumUrl, songUrl, menuChoice){
        const {userName}=this.state;
        return (
            <MainView
                userName={userName}
                widths={widths}
                albumUrl={albumUrl}
                songUrl={songUrl}
                menuChoice={menuChoice}
                onLoginChange={this.onLoginChange}
            />
        );
    }

    render() {
        const {userName}=this.state;
        return (
            <Switch>
                <Route path='/album/:album/:song' render={(props) => {
                    return this.mainView([7, 8, 9], props.match.params.album, props.match.params.song, 'albums');
                }}/>
                <Route path='/album/:album' render={(props) => {
                    return this.mainView([12, 12, 0], props.match.params.album, null, 'albums');
                }}/>
                <Route exact path='/song/:song' render={(props) => {
                    return this.mainView([0, 12, 12], null, props.match.params.song, 'songs');
                }}/>
                <Route exact path='/songs' render={(props) => {
                    return this.mainView([0, 24, 0], null, null, 'songs');
                }}/>
                <Route exact path='/albums' render={(props) => {
                    return this.mainView([24, 0, 0], null, null, 'albums');
                }}/>
                <Route exact path='/' render={(props) => {
                    return this.mainView([24, 0, 0], null, null, 'albums');
                }}/>
            </Switch>
        )
    }
}
