import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form, Input, Button} from 'antd';
const FormItem = Form.Item;

import LoginWidget from '../widget/login'

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.props.form.validateFields();
    }

    submit(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.submit(values);
            }
        });
    }

    render() {
        return <LoginWidget form={this.props.form} submit={this.submit} cancel={this.props.cancel}/>
    }
};

LoginForm.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired
}

export default Form.create({})(LoginForm);
