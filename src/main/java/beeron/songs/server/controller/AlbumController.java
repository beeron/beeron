package beeron.songs.server.controller;

import beeron.songs.server.form.AlbumForm;
import beeron.songs.server.services.AlbumService;
import beeron.songs.server.types.Album;
import beeron.songs.server.types.AlbumExt;
import beeron.songs.server.types.Song;
import beeron.songs.server.types.impl.AlbumImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class AlbumController extends AbstractController {

    private static final Logger logger = Logger.getLogger(AlbumController.class.getName());

    private AlbumService albumService;

    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping(value = "/beeron/albums", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Collection<AlbumExt> albumsJson() {
        Collection<AlbumExt> albums = albumService.getAllAlbums();
        return albums;
    }

    @GetMapping(value = "/beeron/album/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    AlbumExt albumJson(@PathVariable String name) {
        AlbumExt album = albumService.getAlbum(name);
        return album;
    }

    @GetMapping(value = "/beeron/album/{name}/songs", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Collection<Song> songsForAlbum(@PathVariable String name) {
        Collection<Song> songs = albumService.getSongsForAlbum(name);
        return songs;
    }

    @GetMapping(value = "/beeron/album/{name}/available/names", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Collection<String> songNamesNotForAlbum(@PathVariable String name) {
        Collection<String> songNames = albumService.getSongNamesNotForAlbum(name);
        return songNames;
    }

    @PostMapping("/beeron/albums")
    public ResponseEntity<String> addAlbum(Model model, @ModelAttribute @Valid AlbumForm albumForm, BindingResult bindingResult) {
//        if (!authorised(token)) {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//        }
        if (bindingResult.hasFieldErrors()) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(getError(bindingResult));
        }
        Album album = new AlbumImpl(albumForm);
        albumService.addAlbum(album);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/beeron/album/{name}")
    public ResponseEntity<String> updateAlbum(@PathVariable String name, @ModelAttribute @Valid AlbumForm albumForm, BindingResult bindingResult) {
//        if (!authorised(token)) {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//        }
        if (bindingResult.hasFieldErrors()) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(getError(bindingResult));
        }
        Album album = new AlbumImpl(albumForm);
        albumService.updateOrAddAlbum(name, album);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/beeron/album/{name}")
    public ResponseEntity<String> addSongToAlbum(@PathVariable String name, @RequestParam(value = "song", required = true) String song) {
        albumService.addSongToAlbum(name, song);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/beeron/album/{name}")
    public ResponseEntity<String> deleteAlbum(@PathVariable String name) {
//        if (!authorised(token)) {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//        }
        albumService.deleteAlbum(name);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
