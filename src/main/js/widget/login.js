import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form, Input, Button} from 'antd';
const FormItem = Form.Item;
import {hasErrors} from "../common";

const Login=({form, submit, cancel})=>{
    const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched}=form;
    const userNameError = isFieldTouched('name') && getFieldError('name');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    return (
        <div>
            <Form className="login-form" onSubmit={submit}>
                <FormItem
                    validateStatus={userNameError ? 'error' : ''}
                    help={userNameError || ''}>{
                    getFieldDecorator('name', {
                        rules: [{required: true, message: 'please enter a username'}],
                    })(
                        <Input placeholder="Username"/>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={passwordError ? 'error' : ''}
                    help={passwordError || ''}>{
                    getFieldDecorator('password', {
                        rules: [{required: true, message: 'please enter a password'}],
                    })(
                        <Input placeholder="Password"/>
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>Log in</Button>
                </FormItem>
                <FormItem>
                    <Button onClick={cancel} htmlType="reset">Cancel</Button>
                </FormItem>
            </Form>
        </div>
    );
}

Login.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired
}

export default Login;
