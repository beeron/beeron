import '../css/shared.css';
import '../css/antd.css';

import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {BrowserRouter} from 'react-router-dom'
import Main from './container/main'

import store from './store/index';
import { setSongs, setAlbums } from './actions/index';

window.store = store;
window.setAlbums = setAlbums;
window.setSongs = setSongs;

ReactDOM.render(
    <BrowserRouter>
        <Main/>
    </BrowserRouter>,
    document.getElementById('root')
);
