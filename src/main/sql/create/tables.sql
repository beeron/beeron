USE songs
GO

CREATE TABLE song(
    id song_id IDENTITY(1,1) CONSTRAINT pk_song PRIMARY KEY,
    name song_name CONSTRAINT uk_song_name UNIQUE,
    length interval,
    youtube youtube
);
GO

CREATE TABLE album(
    id song_id IDENTITY(1,1) CONSTRAINT pk_album PRIMARY KEY,
    name album_name CONSTRAINT uk_album_name UNIQUE
);
GO

CREATE TABLE song_on_album(
    song_id song_id NOT NULL REFERENCES song ON DELETE CASCADE,
    album_id album_id NOT NULL REFERENCES album ON DELETE CASCADE,
    track INT NOT NULL,
    CONSTRAINT uk_soa_song_album UNIQUE(song_id, album_id),
    CONSTRAINT uk_soa_album_track UNIQUE(album_id, track)
);
GO

CREATE VIEW album_ext AS SELECT
    a.id,
    a.name,
    count(s.id) AS track_count,
    COALESCE(SUM(s.length), 0) AS length
    FROM
    album a LEFT OUTER JOIN (song_on_album soa JOIN song s ON soa.song_id=s.id) ON soa.album_id=a.id AND soa.song_id=s.id AND soa.album_id=a.id
    GROUP BY a.id, a.name;
GO