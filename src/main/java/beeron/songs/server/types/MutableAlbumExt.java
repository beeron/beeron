package beeron.songs.server.types;

import java.time.Duration;

public interface MutableAlbumExt extends AlbumExt {

    void setName(String name);

    void setTrackCount(int trackCount);

    void setLength(Duration length);


}
