import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import {Menu} from 'antd';
const {SubMenu} = Menu;

import {Link} from 'react-router-dom'

const SideMenu=({selected})=>{
    return (
        <Menu
            mode="inline"
            defaultSelectedKeys={[selected]}
            defaultOpenKeys={['music']}
            style={{height: '100%', borderRight: 0}}>
            <SubMenu key="music" title={<span>Music</span>}>
                <Menu.Item key="albums"><Link to='/albums'>Albums</Link></Menu.Item>
                <Menu.Item key="songs"><Link to='/songs'>Songs</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="games" title={<span>Games</span>}>
                <Menu.Item key="pacman">Pacman</Menu.Item>
            </SubMenu>
        </Menu>
    );
}

SideMenu.propTypes={
    selected: PropTypes.string.isRequired
}

export default SideMenu