package beeron.songs.server.services.impl;

import beeron.songs.server.services.SongService;
import beeron.songs.server.types.Song;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

@Repository
public class DbSongService implements SongService {

    private static final Logger logger = Logger.getLogger(DbSongService.class.getName());

    private final DataSource dataSource;

    private static final String RS = "#result-set-1";

//    @PersistenceContext
//    private EntityManager entityManager;

    private final RowMapper<Song> songRowMapper;

    public DbSongService(DataSource dataSource) {
        this.dataSource = dataSource;
        songRowMapper = new SongRowMapper();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Song> getAllSongs() {
        Map<String, Object> result = new SimpleJdbcCall(dataSource).withProcedureName("read_songs").returningResultSet(RS, songRowMapper).execute();
        return (Collection<Song>) result.values().iterator().next();
    }

    @Override
    @Transactional(readOnly = true)
    public Song getSong(String name) {
        Map<String, Object> result = new SimpleJdbcCall(dataSource).withProcedureName("read_song").returningResultSet(RS, songRowMapper).declareParameters(new SqlParameter("NAME", Types.VARCHAR)).execute(name);
        Collection<Song> songs = (Collection<Song>) result.values().iterator().next();
        if (songs.isEmpty()) {
            return null;
        }
        return songs.iterator().next();
    }

    @Override
    @Transactional
    public void addSong(Song song) {
        long seconds = song.getLength().getSeconds();
        new SimpleJdbcCall(dataSource).withProcedureName("create_song").execute(song.getName(), seconds, song.getYoutube());
    }

    @Override
    public void updateOrAddSong(String name, Song song) {
        long seconds = song.getLength().getSeconds();
        new SimpleJdbcCall(dataSource).withProcedureName("update_or_create_song").execute(name, song.getName(), seconds, song.getYoutube());
    }

    @Override
    @Transactional
    public void deleteSong(String name) {
        new SimpleJdbcCall(dataSource).withProcedureName("delete_song").execute(name);
    }
}
