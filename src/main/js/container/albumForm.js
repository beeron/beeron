import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form} from 'antd';
import AlbumWidget from '../widget/album'

class AlbumForm extends React.Component {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.props.form.validateFields();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.name != this.props.name) {
            this.props.form.setFieldsValue({
                name: nextProps.name,
            });
        }
    }

    submit(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.submit(values);
            }
        });
    }

    render() {
        return <AlbumWidget form={this.props.form} submit={this.submit} cancel={this.props.cancel} name={this.props.name}/>
    }
}

AlbumForm.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    name: PropTypes.string
}

export default Form.create({})(AlbumForm)
