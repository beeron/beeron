package beeron.songs.server.services.impl;

import beeron.songs.server.types.Song;
import beeron.songs.server.types.impl.SongImpl;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;

public class SongRowMapper implements RowMapper<Song> {

    @Nullable
    @Override
    public Song mapRow(ResultSet rs, int rowNum) throws SQLException {
        String name = rs.getString("name");
        Duration length = Duration.ofSeconds(rs.getInt("length"));
        String youtube = rs.getString("youtube");
        return new SongImpl(name, length, youtube);
    }
}
