import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {endpoint} from "../common";
import TopMenuWidget from '../widget/topMenu'

export default class TopMenu extends React.Component {
    constructor(props) {
        super(props);
        this.loginOrLogout = this.loginOrLogout.bind(this);
        this.logInFinished = this.logInFinished.bind(this);
        this.logOutFinished = this.logOutFinished.bind(this);
        this.submitLogin = this.submitLogin.bind(this);
        this.cancelLogin = this.cancelLogin.bind(this);
        this.state = {
            loginVisible: false
        };
    }

    cancelLogin() {
        this.setState({loginVisible: false});
    }

    logInFinished(userName) {
        this.setState({loginVisible: false});
        this.props.onLoginChange(userName);
    }

    logOutFinished() {
        this.props.onLoginChange();
    }

    submitLogin(values) {
        values.name = encodeURIComponent(values.name);
        $.ajax({
            type: "POST",
            xhrFields: {
                withCredentials: true
            },
            withCredentials: true,
            url: endpoint+'/login',
            data: values,
            error: function (response) {
                alert(response.responseText);
            },
            success: ()=>this.logInFinished(values.name)
        });
    }

    loginOrLogout() {
        if (this.props.userName) {
            this.props.onLoginChange(null);
        }
        else {
            this.setState({loginVisible: true});
        }
    }

    render() {
        return <TopMenuWidget
            userName={this.props.userName}
            loginVisible={this.state.loginVisible}
            submitLogin={this.submitLogin}
            cancelLogin={this.cancelLogin}
            loginOrLogout={this.loginOrLogout}
        />
    }

};

TopMenu.propTypes={
    userName: PropTypes.string,
    onLoginChange: PropTypes.func.isRequired
}
