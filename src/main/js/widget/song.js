import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form, Input, Button, TimePicker} from 'antd';
const FormItem = Form.Item;
import {hasErrors} from "../common";

const Song=({form, submit, cancel, name, length, youtube})=>{
    const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched}=form;
    const nameError = isFieldTouched('name') && getFieldError('name');
    const lengthError = isFieldTouched('length') && getFieldError('length');
    const youtubeError = isFieldTouched('youtube') && getFieldError('youtube');
    const format = 'HH:mm:ss';
    return (
        <div>
            <Form className="song-form" onSubmit={submit}>
                <FormItem
                    validateStatus={nameError ? 'error' : ''}
                    help={nameError || ''}>{
                    getFieldDecorator('name', {
                        initialValue: name,
                        rules: [{required: true, message: 'please enter a name'}],
                    })(
                        <Input placeholder="name"/>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={lengthError ? 'error' : ''}
                    help={lengthError || ''}>{
                    getFieldDecorator('length', {
                        initialValue: length,
                        rules: [{required: true, message: 'please enter a length'}],
                    })(
                        <TimePicker placeholder="length"
                                    format={format}/>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={youtubeError ? 'error' : ''}
                    help={youtubeError || ''}>{
                    getFieldDecorator('youtube', {
                        initialValue: youtube,
                        rules: [],
                    })(
                        <Input placeholder="youtube"/>
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>Save</Button>
                </FormItem>
                <FormItem>
                    <Button onClick={cancel} htmlType="reset">Cancel</Button>
                </FormItem>
            </Form>
        </div>
    );
}

Song.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    name: PropTypes.string,
    length: PropTypes.object,
    youtube: PropTypes.string
}

export default Song;

