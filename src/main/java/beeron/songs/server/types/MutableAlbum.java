package beeron.songs.server.types;

public interface MutableAlbum extends Album {

    void setName(String name);

}
