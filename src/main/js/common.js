import ReactDOM from 'react-dom';
import React from 'react';

export const SET_ALBUMS = "SET_ALBUMS";
export const SET_SONGS = "SET_SONGS";

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
}

export function toSeconds(time) {
    return parseInt(time.format('HH')) * 60 * 60 + parseInt(time.format('mm')) * 60 + parseInt(time.format('ss'));
}

export function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export const endpoint='http://localhost:8089/beeron';
