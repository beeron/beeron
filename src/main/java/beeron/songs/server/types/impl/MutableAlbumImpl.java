package beeron.songs.server.types.impl;

import beeron.songs.server.types.Album;
import beeron.songs.server.types.MutableAlbum;

import java.util.Objects;

public class MutableAlbumImpl extends AlbumImpl implements MutableAlbum {

    public MutableAlbumImpl(String name) {
        super(name);
    }

    public MutableAlbumImpl(Album album) {
        super(album);
    }

    @Override
    public void setName(String name) {
        Objects.requireNonNull(name);
        this.name = name;
    }

}
