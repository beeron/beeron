import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form, Input, Button} from 'antd';
const FormItem = Form.Item;
import {hasErrors} from "../common";

const Album=({form, submit, cancel, name})=>{
    const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched}=form;
    const nameError = isFieldTouched('name') && getFieldError('name');
    return (
        <Form className="album-form" onSubmit={submit}>
            <FormItem
                validateStatus={nameError ? 'error' : ''}
                help={nameError || ''}>{
                getFieldDecorator('name', {
                    initialValue: name,
                    rules: [{required: true, message: 'please enter a name'}],
                })(
                    <Input placeholder="name"/>
                )}
            </FormItem>
            <FormItem>
                <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>Save</Button>
            </FormItem>
            <FormItem>
                <Button onClick={cancel} htmlType="reset">Cancel</Button>
            </FormItem>
        </Form>
    );
}

Album.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired
}

export default Album;
