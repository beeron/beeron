package beeron.songs.server.services;

import beeron.songs.server.types.Song;

import java.util.Collection;

public interface SongService {

    Collection<Song> getAllSongs();

    Song getSong(String name);

    void addSong(Song song);

    void deleteSong(String name);

    void updateOrAddSong(String name, Song song);

}
