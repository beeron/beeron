package beeron.songs.server.controller;

import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;

public class AbstractController {

    private static final String LOGGED_IN = "loggedIn";

    protected boolean authorised(String token) {
        return !StringUtils.isEmpty(token);
    }

    protected String getError(BindingResult bindingResult) {
        return bindingResult.getFieldError().getField() + " : " + bindingResult.getFieldError().getDefaultMessage();
    }

}
