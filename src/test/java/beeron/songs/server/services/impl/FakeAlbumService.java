package beeron.songs.server.services.impl;

import beeron.songs.server.services.AlbumService;
import beeron.songs.server.types.Album;
import beeron.songs.server.types.AlbumExt;
import beeron.songs.server.types.Song;
import beeron.songs.server.types.impl.AlbumExtImpl;
import beeron.songs.server.types.impl.SongImpl;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class FakeAlbumService implements AlbumService {

    private static final AlbumExt[] ALBUMS = new AlbumExt[]{new AlbumExtImpl("Volume 1", 10, Duration.ofHours(1))};

    @Override
    public Collection<AlbumExt> getAllAlbums() {
        return Arrays.asList(ALBUMS);
    }

    @Override
    public AlbumExt getAlbum(String name) {
        return ALBUMS[0];
    }

    @Override
    public Collection<Song> getSongsForAlbum(String name) {
        return Collections.singleton(new SongImpl("Horny", Duration.ofMinutes(6)));
    }

    @Override
    public Collection<String> getSongNamesNotForAlbum(String name) {
        return Collections.singleton("Beerontee");
    }

    @Override
    public void addAlbum(Album album) {
    }

    @Override
    public void deleteAlbum(String name) {
    }

    @Override
    public void updateOrAddAlbum(String name, Album album) {
    }

    @Override
    public void addSongToAlbum(String albumName, String songName) {
    }
}
