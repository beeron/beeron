import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Modal, Button, Table} from 'antd';

import SongForm from '../container/songForm'
import SongSelectForm from '../container/songSelectForm'
import SongLink from './songLink'

const SongTable=({
        items, loggedIn, album, editVisible, editName, editLength, editYoutube, selectVisible,
        onRemoveSong, onDeleteSong, onEditSong, onAddSong, onCreateSong,
        onCancelEdit, onSubmitEdit, onCancelSelect, onSubmitSelect})=> {
    if (items === null) {
        return null;
    }
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <SongLink album={album} name={text}/>
        },
        {
            title: 'Length',
            dataIndex: 'length',
            key: 'length',
            render: text => text.toString().toHHMMSS()
        }
    ]
    if (loggedIn) {
        if (album != null) {
            columns.push({
                title: '',
                key: 'remove',
                render: (text, record) => (
                    <a href="#" onClick={(event) => onRemoveSong(event, record)}>Remove</a>
                )
            });

        }
        else {
            columns.push({
                title: '',
                key: 'delete',
                render: (text, record) => (
                    <a href="#" onClick={(event) => onDeleteSong(event, record.name)}>Delete</a>
                )
            });
            columns.push({
                title: '',
                key: 'edit',
                render: (text, record) => {
                    return (
                        <a href="#"
                           onClick={(event) => onEditSong(event, record.name, record.length, record.youtube)}>Edit</a>
                    );
                }
            });
        }
    }
    return (
        <React.Fragment>
            <Modal footer={null} title='Song' visible={editVisible}>
                <SongForm submit={onSubmitEdit} cancel={onCancelEdit} name={editName} length={editLength}
                          youtube={editYoutube}/>
            </Modal>
            {album &&
                <Modal footer={null} title='Select Song' visible={selectVisible}>
                    <SongSelectForm submit={onSubmitSelect} cancel={onCancelSelect} album={album}/>
                </Modal>
            }
            <div className='container'>
                <Table rowKey="name" columns={columns} dataSource={items}/>
                {loggedIn && album &&
                <Button style={{margin: '5px'}} className="editable-add-btn" onClick={onAddSong}>Add song to Album</Button>}
                {loggedIn && !album &&
                <Button style={{margin: '5px'}} className="editable-add-btn" onClick={onCreateSong}>Create New Song</Button>}
            </div>
        </React.Fragment>
    );
}

SongTable.propTypes={
    items: PropTypes.array,
    loggedIn: PropTypes.bool.isRequired,
    album: PropTypes.string,
    editVisible: PropTypes.bool.isRequired,
    editName: PropTypes.string,
    editLength: PropTypes.number,
    editYoutube: PropTypes.string,
    selectVisible: PropTypes.bool.isRequired,
    onRemoveSong: PropTypes.func.isRequired,
    onDeleteSong: PropTypes.func.isRequired,
    onEditSong: PropTypes.func.isRequired,
    onAddSong: PropTypes.func.isRequired,
    onCreateSong: PropTypes.func.isRequired,
    onCancelEdit: PropTypes.func.isRequired,
    onSubmitEdit: PropTypes.func.isRequired,
    onCancelSelect: PropTypes.func.isRequired,
    onSubmitSelect: PropTypes.func.isRequired
}

export default SongTable