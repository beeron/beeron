import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form} from 'antd';
import moment from 'moment';
import SongWidget from '../widget/song'

class SongForm extends React.Component {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.name != this.props.name || nextProps.length != this.props.length || nextProps.youtube != this.props.youtube) {
            nextProps.form.setFieldsValue({
                name: nextProps.name,
                length: this.secondsToMoment(nextProps.length),
                youtube: nextProps.youtube
            });
        }
    }

    secondsToMoment(length) {
        const format = 'HH:mm:ss';
        if (length == null) {
            return moment('00:00', format);
        }
        return moment(length.toString().toHHMMSS(), format);
    }

    componentDidMount() {
        this.props.form.validateFields();
    }

    submit(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.submit(values);
            }
        });
    }

    render() {
        return <SongWidget form={this.props.form} submit={this.submit} cancel={this.props.cancel}/>
    }
}

SongForm.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired
}

export default Form.create({})(SongForm);

