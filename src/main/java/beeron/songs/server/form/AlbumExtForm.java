package beeron.songs.server.form;

import beeron.songs.server.types.AlbumExt;
import org.springframework.core.convert.ConversionService;

import java.time.Duration;

public class AlbumExtForm implements AlbumExt {

    private AlbumExt albumExt;
    private String sLength;

    public AlbumExtForm(ConversionService conversionService, AlbumExt albumExt) {
        this.albumExt = albumExt;
        sLength = conversionService.convert(albumExt.getLength(), String.class);
    }

    @Override
    public String getName() {
        return albumExt.getName();
    }

    @Override
    public int getTrackCount() {
        return albumExt.getTrackCount();
    }

    @Override
    public Duration getLength() {
        return albumExt.getLength();
    }

    public String getStringLength() {
        return sLength;
    }
}
