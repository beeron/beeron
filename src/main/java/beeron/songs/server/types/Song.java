package beeron.songs.server.types;

import java.time.Duration;

public interface Song {

    String getName();

    Duration getLength();

    String getYoutube();
}
