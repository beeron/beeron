package beeron.songs.server.services.impl;


import beeron.songs.server.services.SongService;
import beeron.songs.server.types.Song;
import beeron.songs.server.types.impl.SongImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SongServiceTest {

    private static final String SONG_NAME = "test album";
    private static final Duration SONG_DURATION = Duration.ofMinutes(5);

    @Autowired
    private SongService songService;

    @Before
    public void resetDb() {
        songService.deleteSong(SONG_NAME);
    }

    @Test
    public void testCrud() {
        Song createSong = new SongImpl(SONG_NAME, SONG_DURATION);
        songService.addSong(createSong);
        Song getSong = songService.getSong(SONG_NAME);
        Assert.assertEquals(createSong.getName(), getSong.getName());
        Assert.assertEquals(createSong.getLength(), getSong.getLength());
        songService.deleteSong(SONG_NAME);
        getSong = songService.getSong(SONG_NAME);
        Assert.assertNull(getSong);
    }
}
