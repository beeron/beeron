package beeron.songs.server.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.Formatter;
import org.springframework.lang.Nullable;

import java.text.ParseException;
import java.time.Duration;
import java.util.Locale;

public class DurationFormatter implements Formatter<Duration>, Converter<Duration, String> {

    private final Converter<String, Duration> converter = new DurationConverter();

    @Override
    public String print(Duration duration, Locale locale) {
        return print(duration, false);
    }

    public String print(Duration duration, boolean shortFormat) {
        StringBuilder sb = new StringBuilder();
        long days = duration.toDays();
        if (days != 0 || !shortFormat) {
            sb.append(days);
            sb.append(' ');
        }
        duration = duration.minusDays(days);
        long hours = duration.toHours();
        if (hours != 0 || !shortFormat) {
            sb.append(hours);
            sb.append(':');
        }
        duration = duration.minusHours(hours);
        long minutes = duration.toMinutes();
        sb.append(String.format("%02d", minutes));
        sb.append(':');
        duration = duration.minusMinutes(minutes);
        long seconds = duration.getSeconds();
        sb.append(String.format("%02d", seconds));
        duration = duration.minusSeconds(seconds);
        long millis = duration.toMillis();
        if (millis != 0 || !shortFormat) {
            sb.append('.');
            sb.append(millis);
        }
        return sb.toString();
    }

    @Override
    public Duration parse(String text, Locale locale) throws ParseException {
        try {
            return converter.convert(text);
        } catch (IllegalArgumentException e) {
            throw new ParseException(text, 0);
        }
    }

    @Nullable
    @Override
    public String convert(Duration duration) {
        return print(duration, Locale.getDefault());
    }
}
