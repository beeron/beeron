import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import LoginForm from '../container/loginForm'
import {Modal, Button} from 'antd';

const TopMenu=({userName, loginVisible, submitLogin, cancelLogin, loginOrLogout})=>{
    return (
        <div>
            <Modal footer={null} title='Login' visible={loginVisible}>
                <LoginForm submit={submitLogin} cancel={cancelLogin}/>
            </Modal>
            <span>{userName && 'Logged in as ' + userName}</span>
            <span className='separator'/>
            <Button onClick={loginOrLogout}>{userName ? 'Logout' : 'Login'}</Button>
            <span className='separator'/>
            <span className='title'>Beeron</span>
        </div>
    );
}

TopMenu.propTypes={
    userName: PropTypes.string,
    loginVisible: PropTypes.bool.isRequired,
    submitLogin: PropTypes.func.isRequired,
    cancelLogin: PropTypes.func.isRequired,
    loginOrLogout: PropTypes.func.isRequired
}

export default TopMenu