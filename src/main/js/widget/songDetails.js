import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import Youtube from './youtube.js'

const SongDetails=({song, error, isLoaded, name, length, youtube})=>{
    if (song == null) {
        return null;
    }
    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <div className='container'>
                <table>
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>{name}</td>
                    </tr>
                    <tr>
                        <td>Length:</td>
                        <td>{length}</td>
                    </tr>
                    </tbody>
                </table>
                <Youtube id={youtube}/>
            </div>
        );
    }
}

SongDetails.propTypes={
    song: PropTypes.string,
    error: PropTypes.object,
    isLoaded: PropTypes.bool.isRequired,
    name: PropTypes.string,
    length: PropTypes.string,
    youtube: PropTypes.string
}

export default SongDetails