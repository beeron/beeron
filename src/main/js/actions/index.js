import { SET_ALBUMS } from "../common";
import { SET_SONGS } from "../common";

export const setAlbums = albums => ({ type: SET_ALBUMS, payload: albums });
export const setSongs = songs => ({ type: SET_SONGS, payload: songs });