package beeron.songs.server.types.impl;

import beeron.songs.server.types.AlbumExt;

import java.time.Duration;
import java.util.Objects;

public class AlbumExtImpl extends AlbumImpl implements AlbumExt {

    protected int trackCount;
    protected Duration length;

    public AlbumExtImpl(String name, int trackCount, Duration length) {
        super(name);
        Objects.requireNonNull(length);
        this.trackCount = trackCount;
        this.length = length;
    }

    public AlbumExtImpl(AlbumExt albumExt) {
        super(albumExt);
        trackCount = albumExt.getTrackCount();
        length = albumExt.getLength();
    }

    @Override
    public int getTrackCount() {
        return trackCount;
    }

    @Override
    public Duration getLength() {
        return length;
    }
}
