package beeron.songs.server.types.impl;

import beeron.songs.server.types.MutableSong;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;

public class MutableSongImplTest {

    private MutableSong song;

    private static final String NAME = "name";
    private static final Duration LENGTH = Duration.ofMinutes(6);

    @Before
    public void setUp() {
        song = new MutableSongImpl(NAME, LENGTH);
    }

    @Test
    public void testAttributes() {
        Assert.assertEquals(song.getName(), NAME);
        Assert.assertEquals(song.getLength(), LENGTH);

        final String ALT_NAME = "alt name";
        final Duration ALT_LENGTH = Duration.ofMinutes(4);
        song.setName(ALT_NAME);
        song.setLength(ALT_LENGTH);

        Assert.assertEquals(song.getName(), ALT_NAME);
        Assert.assertEquals(song.getLength(), ALT_LENGTH);
    }

    @Test(expected = Exception.class)
    public void testNullName() {
        song.setName(null);
    }

    @Test(expected = Exception.class)
    public void testNullPublished() {
        song.setLength(null);
    }

}
