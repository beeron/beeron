import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import {Link} from 'react-router-dom'

const AlbumLink=({name})=> {
    return (
        <Link to={'/album/'+name}>{name}</Link>
    );
}

AlbumLink.propTypes={
    name: PropTypes.string.isRequired
}

export default AlbumLink