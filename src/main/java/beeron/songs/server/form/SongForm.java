package beeron.songs.server.form;

import beeron.songs.server.types.MutableSong;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Duration;

public class SongForm implements MutableSong {

    @NotEmpty
    private String name;
    @NotNull
    private Duration length;
    private String youtube;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Duration getLength() {
        return length;
    }

    @Override
    public void setLength(Duration length) {
        this.length = length;
    }

    @Override
    public String getYoutube() {
        return youtube;
    }

    @Override
    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }
}
