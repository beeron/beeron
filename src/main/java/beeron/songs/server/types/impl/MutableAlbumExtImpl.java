package beeron.songs.server.types.impl;

import beeron.songs.server.types.MutableAlbumExt;

import java.time.Duration;

public class MutableAlbumExtImpl extends AlbumExtImpl implements MutableAlbumExt {

    public MutableAlbumExtImpl(String name, int trackCount, Duration length) {
        super(name, trackCount, length);
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setTrackCount(int trackCount) {
        this.trackCount = trackCount;
    }

    @Override
    public void setLength(Duration length) {
        this.length = length;
    }
}
