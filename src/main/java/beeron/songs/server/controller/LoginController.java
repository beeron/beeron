package beeron.songs.server.controller;

import beeron.songs.server.form.LoginForm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class LoginController extends AbstractController {

    private static final Logger logger = Logger.getLogger(LoginController.class.getName());

    @PostMapping(path = "/beeron/login")
    public ResponseEntity<String> loginSubmit(Model model, @ModelAttribute @Valid LoginForm loginForm, BindingResult bindingResult, HttpServletResponse response) {
        if (bindingResult.hasFieldErrors()) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(getError(bindingResult));
        }
//        String token = loginForm.getName();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
