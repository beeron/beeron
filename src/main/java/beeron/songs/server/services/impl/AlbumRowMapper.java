package beeron.songs.server.services.impl;

import beeron.songs.server.types.AlbumExt;
import beeron.songs.server.types.impl.AlbumExtImpl;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;

public class AlbumRowMapper implements RowMapper<AlbumExt> {

    @Nullable
    @Override
    public AlbumExt mapRow(ResultSet rs, int rowNum) throws SQLException {
        String name = rs.getString("name");
        int trackCount = rs.getInt("track_count");
        Duration length = Duration.ofSeconds(rs.getInt("length"));
        return new AlbumExtImpl(name, trackCount, length);
    }
}
