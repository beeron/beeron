package beeron.songs.server.form;

import beeron.songs.server.types.MutableAlbum;

import javax.validation.constraints.NotNull;

public class AlbumForm implements MutableAlbum {

    @NotNull
    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

}
