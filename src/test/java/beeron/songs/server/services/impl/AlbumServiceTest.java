package beeron.songs.server.services.impl;


import beeron.songs.server.services.AlbumService;
import beeron.songs.server.types.Album;
import beeron.songs.server.types.impl.AlbumImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AlbumServiceTest {

    private static final String ALBUM_NAME = "test album";

    @Autowired
    private AlbumService albumService;

    @Before
    public void resetDb() {
        albumService.deleteAlbum(ALBUM_NAME);
    }

    @Test
    public void testCrud() {
        Album createAlbum = new AlbumImpl(ALBUM_NAME);
        albumService.addAlbum(createAlbum);
        Album getAlbum = albumService.getAlbum(ALBUM_NAME);
        Assert.assertEquals(createAlbum.getName(), getAlbum.getName());
        albumService.deleteAlbum(ALBUM_NAME);
        getAlbum = albumService.getAlbum(ALBUM_NAME);
        Assert.assertNull(getAlbum);
    }
}
