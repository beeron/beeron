package beeron.songs.server;

import beeron.songs.server.converter.DurationConverter;
import beeron.songs.server.converter.DurationFormatter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.Formatter;

import java.time.Duration;

@SpringBootApplication
public class ApplicationConfig {

    @Bean
    @ConfigurationPropertiesBinding
    public Converter<String, Duration> durationConverter() {
        return new DurationConverter();
    }

    @Bean
    @ConfigurationPropertiesBinding
    public Formatter<Duration> durationFormatter() {
        return new DurationFormatter();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApplicationConfig.class, args);
    }
}
