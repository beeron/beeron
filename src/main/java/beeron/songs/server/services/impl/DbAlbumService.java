package beeron.songs.server.services.impl;

import beeron.songs.server.services.AlbumService;
import beeron.songs.server.types.Album;
import beeron.songs.server.types.AlbumExt;
import beeron.songs.server.types.Song;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

@Repository
public class DbAlbumService implements AlbumService {

    private static final Logger logger = Logger.getLogger(DbAlbumService.class.getName());

    private final DataSource dataSource;

    private final RowMapper<AlbumExt> albumRowMapper;
    private final RowMapper<Song> songRowMapper;

    private static final String RS = "#result-set-1";

    public DbAlbumService(DataSource dataSource) {
        this.dataSource = dataSource;
        albumRowMapper = new AlbumRowMapper();
        songRowMapper = new SongRowMapper();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<AlbumExt> getAllAlbums() {
        Map<String, Object> result = new SimpleJdbcCall(dataSource).withProcedureName("read_albums").returningResultSet(RS, albumRowMapper).execute();
        return (Collection<AlbumExt>) result.values().iterator().next();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Song> getSongsForAlbum(String name) {
        Map<String, Object> result = new SimpleJdbcCall(dataSource).withProcedureName("read_songs_for_album").returningResultSet(RS, songRowMapper).declareParameters(new SqlParameter("NAME", Types.VARCHAR)).execute(name);
        return (Collection<Song>) result.values().iterator().next();
    }

    @Override
    public Collection<String> getSongNamesNotForAlbum(String name) {
        RowMapper<String> rowMapper = (ResultSet rs, int rowNum) -> rs.getString(1);
        Map<String, Object> result = new SimpleJdbcCall(dataSource).withProcedureName("read_song_names_not_for_album").returningResultSet(RS, rowMapper).declareParameters(new SqlParameter("NAME", Types.VARCHAR)).execute(name);
        return (Collection<String>) result.values().iterator().next();
    }

    @Override
    @Transactional(readOnly = true)
    public AlbumExt getAlbum(String name) {
        Map<String, Object> result = new SimpleJdbcCall(dataSource).withProcedureName("read_album").returningResultSet(RS, albumRowMapper).declareParameters(new SqlParameter("NAME", Types.VARCHAR)).execute(name);
        Collection<AlbumExt> albums = (Collection<AlbumExt>) result.values().iterator().next();
        if (albums.isEmpty()) {
            return null;
        }
        return albums.iterator().next();
    }

    @Override
    @Transactional
    public void addAlbum(Album album) {
        new SimpleJdbcCall(dataSource).withProcedureName("create_album").execute(album.getName());
    }

    @Override
    public void updateOrAddAlbum(String name, Album album) {
        new SimpleJdbcCall(dataSource).withProcedureName("update_or_create_album").execute(name, album.getName());
    }

    @Override
    @Transactional
    public void deleteAlbum(String name) {
        new SimpleJdbcCall(dataSource).withProcedureName("delete_album").execute(name);
    }

    @Override
    @Transactional
    public void addSongToAlbum(String albumName, String songName) {
        new SimpleJdbcCall(dataSource).withProcedureName("add_song_to_album_end").execute(albumName, songName);
    }
}
