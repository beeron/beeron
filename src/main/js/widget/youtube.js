import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

const Youtube=({id})=> {
    if (id == null) {
        return (
            <p>No Youtube video</p>
        );
    }
    const href = "https://www.youtube.com/watch?v=" + id + "?autoplay=1";
    const src = "https://www.youtube.com/embed/" + id;
    return (
        <div>
            <a href={href} target="_blank">Go to Youtube page</a>
            <div>
                <iframe src={src} width="560" height="315" frameBorder="0" allowFullScreen></iframe>
            </div>
        </div>
    );
}

Youtube.propTypes={
    id: PropTypes.string
}

export default Youtube