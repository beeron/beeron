import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {Form, Input, Button, Select} from 'antd';
const FormItem = Form.Item;

const SongSelect=({form, submit, cancel, items})=>{
    const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched}=form;
    const selectItems=items.map((item)=>
        <Select.Option key={item} value={item}>{item}</Select.Option>
    );
    const songError = isFieldTouched('song') && getFieldError('song');
    return (
        <div>
            <Form className="song-form" onSubmit={submit}>
                <FormItem
                    validateStatus={songError ? 'error' : ''}
                    help={songError || ''}>{
                    getFieldDecorator('song', {
                        initialValue: items[0],
                        rules: [{required: true, message: 'please enter a song'}],
                    })(
                        <Select>
                            {selectItems}
                        </Select>
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit">Save</Button>
                </FormItem>
                <FormItem>
                    <Button onClick={cancel} htmlType="reset">Cancel</Button>
                </FormItem>
            </Form>
        </div>
    );
}

SongSelect.propTypes={
    submit: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    items: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default SongSelect;

