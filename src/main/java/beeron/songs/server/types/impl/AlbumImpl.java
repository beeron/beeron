package beeron.songs.server.types.impl;

import beeron.songs.server.types.Album;

import java.util.Objects;

public class AlbumImpl implements Album {
    protected String name;

    public AlbumImpl(String name) {
        Objects.requireNonNull(name);
        this.name = name;
    }

    public AlbumImpl(Album album) {
        name = album.getName();
    }

    @Override
    public String getName() {
        return name;
    }
}
