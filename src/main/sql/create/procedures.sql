USE songs
GO

CREATE FUNCTION seconds_to_time(@seconds INT) RETURNS TIME AS
BEGIN
    RETURN CONVERT(TIME, DATEADD(SECOND, @seconds, 0))
END
GO

CREATE FUNCTION time_to_seconds(@time TIME) RETURNS INT AS
BEGIN
    RETURN DATEDIFF(SECOND, 0, CAST(@time AS DATETIME))
END
GO

CREATE PROCEDURE add_song_to_album (@album_name album_name, @track INT, @song_name song_name) AS
BEGIN
    INSERT INTO song_on_album (song_id, album_id, track) VALUES (
        (SELECT id FROM song WHERE name=@song_name),
        (SELECT id FROM album WHERE name=@album_name),
        @track
    )
END
GO

CREATE PROCEDURE add_song_to_album_end (@album_name album_name, @song_name song_name) AS
BEGIN
    INSERT INTO song_on_album(song_id, album_id, track) VALUES (
        (SELECT id FROM song WHERE name=@song_name),
        (SELECT id FROM album WHERE name=@album_name),
        (SELECT COALESCE(MAX(track)+1, 1) FROM song_on_album WHERE album_id=(SELECT id FROM album WHERE name=@album_name))
    )
END
GO

CREATE PROCEDURE create_song (@name song_name, @length interval, @youtube youtube=NULL) AS
BEGIN
    INSERT INTO song(name, length, youtube) VALUES (@name, @length, @youtube);
END;
GO

CREATE PROCEDURE create_song2 (@name song_name, @length varchar(64), @youtube youtube=NULL) AS
BEGIN
    DECLARE @length_int interval
    SET @length_int=dbo.time_to_seconds(CAST(@length AS TIME))
    INSERT INTO song(name, length, youtube) VALUES (@name, @length_int, @youtube);
END;
GO

CREATE PROCEDURE update_or_create_song (@old_name song_name, @new_name song_name, @new_length interval, @new_youtube youtube=NULL) AS
BEGIN
    UPDATE song SET name=@new_name, length=@new_length, youtube=@new_youtube WHERE name=@old_name
    IF @@ROWCOUNT=0
        EXEC create_song @new_name, @new_length, @new_youtube
END
GO

CREATE PROCEDURE delete_song (@name song_name) AS
BEGIN
    DELETE FROM song WHERE name=@name
END
GO

CREATE PROCEDURE create_album (@name album_name) AS
BEGIN
    INSERT INTO album(name) VALUES (@name)
END
GO

CREATE PROCEDURE update_or_create_album (@old_name album_name, @new_name album_name) AS
BEGIN
    UPDATE album SET name=@new_name WHERE name=@old_name
    IF @@ROWCOUNT=0
        EXEC create_album @new_name
END
GO

CREATE PROCEDURE delete_album (@name album_name) AS
BEGIN
    DELETE FROM album WHERE name=@name
END
GO

CREATE PROCEDURE read_albums AS
    SELECT name, track_count, length FROM album_ext ORDER BY name
GO

CREATE PROCEDURE read_album (@name album_name) AS
    SELECT name, track_count, length FROM album_ext WHERE name=@name
GO

CREATE PROCEDURE read_songs_for_album (@name album_name) AS
    SELECT s.name, s.length, s.youtube from song s, song_on_album soa, album a WHERE soa.song_id=s.id and soa.album_id=a.id AND a.name=@name ORDER BY soa.track
GO

CREATE PROCEDURE read_song_names_not_for_album (@name album_name) AS
    SELECT s.name from song s, song_on_album soa, album a WHERE soa.song_id=s.id and soa.album_id=a.id AND a.name<>@name ORDER BY s.name
GO

CREATE PROCEDURE read_songs AS
    SELECT name, length, youtube FROM song ORDER BY name
GO

CREATE PROCEDURE read_song (@name song_name) AS
BEGIN
    SELECT name, length, youtube FROM song WHERE name=@name
END
GO
