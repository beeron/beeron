package beeron.songs.server.types.impl;

import beeron.songs.server.types.Song;

import java.time.Duration;
import java.util.Objects;

public class SongImpl implements Song {
    protected String name;
    protected Duration length;
    protected String youtube;

    public SongImpl(String name, Duration length) {
        this(name, length, null);
    }

    public SongImpl(String name, Duration length, String youtube) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(length);
        this.name = name;
        this.length = length;
        this.youtube = youtube;
    }

    public SongImpl(Song song) {
        name = song.getName();
        length = song.getLength();
        youtube = song.getYoutube();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Duration getLength() {
        return length;
    }

    @Override
    public String getYoutube() {
        return youtube;
    }
}
