package beeron.songs.server.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.time.Duration;

public class DurationConverter implements Converter<String, Duration> {

    @Nullable
    @Override
    public Duration convert(String source) {
        int days = 0;
        int dayIndex = source.lastIndexOf(' ');
        if (dayIndex != -1) {
            String sDays = source.substring(0, dayIndex);
            days = Integer.valueOf(sDays);
            source = source.substring(dayIndex + 1);
        }
        int hours = 0;
        int firstColon = source.indexOf(':');
        int lastColon = source.lastIndexOf(':');
        if (lastColon != firstColon) {
            int hoursIndex = firstColon;
            if (hoursIndex != -1) {
                String sHours = source.substring(0, hoursIndex);
                hours = Integer.valueOf(sHours);
                source = source.substring(hoursIndex + 1);
            }
        }
        int minutes = 0;
        int minutesIndex = source.indexOf(':');
        if (minutesIndex != -1) {
            String sMinutes = source.substring(0, minutesIndex);
            minutes = Integer.valueOf(sMinutes);
            source = source.substring(minutesIndex + 1);
        }
        int seconds = 0;
        int secondsIndex = source.indexOf('.');
        if (secondsIndex == -1) {
            secondsIndex = source.length();
        }
        if (secondsIndex != -1) {
            String sSeconds = source.substring(0, secondsIndex);
            seconds = Integer.valueOf(sSeconds);
        }
        int millis = 0;
        int millisIndex = source.indexOf('.');
        if (millisIndex != -1) {
            String sMillis = source.substring(millisIndex + 1);
            millis = Integer.valueOf(sMillis);
        }
        return Duration.ofDays(days).plusHours(hours).plusMinutes(minutes).plusSeconds(seconds).plusMillis(millis);
    }
}
