import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'
import {endpoint} from "../common";

import SongDetailsWidget from '../widget/songDetails'

export default class SongDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            name: null,
            length: null,
            youtube: null
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.song != nextProps.song) {
            this.reload(nextProps.song);
        }
    }

    reload(song) {
        if (song != null) {
            fetch(endpoint+'/song/' + song + '.json')
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            name: result.name,
                            length: result.length.toString().toHHMMSS(),
                            youtube: result.youtube
                        });
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    })
        }
    }

    componentDidMount() {
        this.reload(this.props.song);
    }

    render() {
        const {error, isLoaded, name, length, youtube} = this.state;
        const {song} = this.props;
        return <SongDetailsWidget song={song} error={error} isLoaded={isLoaded} name={name} length={length} youtube={youtube}/>
    }
}

SongDetails.propTypes={
    song: PropTypes.string
}

