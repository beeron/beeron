package beeron.songs.server.types.impl;

import beeron.songs.server.types.MutableSong;
import beeron.songs.server.types.Song;

import java.time.Duration;
import java.util.Objects;

public class MutableSongImpl extends SongImpl implements MutableSong {

    public MutableSongImpl(String name, Duration length) {
        this(name, length, null);
    }

    public MutableSongImpl(String name, Duration length, String youtube) {
        super(name, length, youtube);
    }

    public MutableSongImpl(Song song) {
        super(song);
    }

    @Override
    public void setName(String name) {
        Objects.requireNonNull(name);
        this.name = name;
    }

    @Override
    public void setLength(Duration length) {
        Objects.requireNonNull(length);
        this.length = length;
    }

    @Override
    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }
}
