import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types'

import {Modal, Button, Table} from 'antd';

import AlbumForm from '../container/albumForm'
import AlbumLink from './albumLink'

const AlbumTable=({
        items, loggedIn, editVisible, editName,
        onDeleteAlbum, onEditAlbum, onAddAlbum, onCancelEdit, onSubmitEdit})=> {
    if (items === null) {
        return null;
    }
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <AlbumLink name={text}/>
        },
        {
            title: 'Tracks',
            dataIndex: 'trackCount',
            key: 'trackCount'
        },
        {
            title: 'Length',
            dataIndex: 'length',
            key: 'length',
            render: text => text.toString().toHHMMSS()
        }
    ]
    if (loggedIn) {
        columns.push({
            title: '',
            key: 'delete',
            render: (text, record) => (
                <a href="#" onClick={(event) => {event.preventDefault();onDeleteAlbum(record.name);}}>Delete</a>
            )
        });
        columns.push({
            title: '',
            key: 'edit',
            render: (text, record) => {
                return (
                    <a href="#" onClick={(event) => {event.preventDefault();onEditAlbum(record.name);}}>Edit</a>
                );
            }
        });
    }
    return (
        <React.Fragment>
            <Modal footer={null} title='Album' visible={editVisible}>
                <AlbumForm submit={onSubmitEdit} cancel={onCancelEdit} name={editName}/>
            </Modal>
            <div className='container'>
                <Table rowKey="name" columns={columns} dataSource={items}/>
                {loggedIn &&
                <Button style={{margin: '5px'}} className="editable-add-btn" onClick={onAddAlbum}>Create New Album</Button>}
            </div>
        </React.Fragment>
    );

}

AlbumTable.propTypes={
    items: PropTypes.array,
    loggedIn: PropTypes.bool.isRequired,
    editVisible: PropTypes.bool.isRequired,
    editName: PropTypes.string,
    onDeleteAlbum: PropTypes.func.isRequired,
    onEditAlbum: PropTypes.func.isRequired,
    onAddAlbum: PropTypes.func.isRequired,
    onCancelEdit: PropTypes.func.isRequired,
    onSubmitEdit: PropTypes.func.isRequired
}

export default AlbumTable