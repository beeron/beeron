package beeron.songs.server.types;

import java.time.Duration;

public interface MutableSong extends Song {

    void setName(String name);

    void setLength(Duration length);

    void setYoutube(String youtube);

}
