package beeron.songs.server.services;

import beeron.songs.server.types.Album;
import beeron.songs.server.types.AlbumExt;
import beeron.songs.server.types.Song;

import java.util.Collection;

public interface AlbumService {

    Collection<AlbumExt> getAllAlbums();

    Collection<Song> getSongsForAlbum(String name);

    Collection<String> getSongNamesNotForAlbum(String name);

    AlbumExt getAlbum(String name);

    void addAlbum(Album album);

    void deleteAlbum(String name);

    void updateOrAddAlbum(String name, Album album);

    void addSongToAlbum(String albumName, String songName);

}
