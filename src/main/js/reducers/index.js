import { SET_ALBUMS } from '../common';
import { SET_SONGS } from '../common';

const initialState = {
  albums: [],
  songs: []
};

const rootReducer = (state = initialState, action) => {
    console.log('state:'+JSON.stringify(state));
    console.log('action:'+action);
    switch(action.type){
        case SET_ALBUMS:{
                let newState={...state, albums: action.payload};
                console.log('new state:'+JSON.stringify(newState));
                return newState;
            }
        case SET_SONGS:{
                let newState={...state, songs: action.payload};
                console.log('new state:'+JSON.stringify(newState));
                return newState;
            }
        default:
            return state;
    }
};

export default rootReducer;